package com.gitlab.isalgueiro.examples.jaxws.server;

import javax.xml.ws.Endpoint;

public class Main {

    /**
     * Plain HTTP server.
     *
     * @param args
     */
    public static void main(String[] args) {
        Endpoint.publish("http://0.0.0.0:9876/service", new HelloPortType());
    }
}
