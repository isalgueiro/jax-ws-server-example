package com.gitlab.isalgueiro.examples.jaxws.server;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.xml.ws.Endpoint;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.security.KeyStore;
import java.security.SecureRandom;

public class HttpsMain {

    /**
     * HTTPS server.
     *
     * @param args
     */
    public static void main(String[] args) {
        final String hostname = "0.0.0.0";
        final int port = 9876;
        try (
                final InputStream keyStoreInputStream = ClassLoader.getSystemResourceAsStream("test.jks")
        ) {
            final Endpoint endpoint = Endpoint.create(new HelloPortType());
            final SSLContext ssl = SSLContext.getInstance("TLS");
            final KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            final KeyStore store = KeyStore.getInstance("JKS");
            store.load(keyStoreInputStream, "password".toCharArray());
            kmf.init(store, "password".toCharArray());
            final KeyManager[] keyManagers = kmf.getKeyManagers();
            final TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(store);
            final TrustManager[] trustManagers = tmf.getTrustManagers();
            ssl.init(keyManagers, trustManagers, new SecureRandom());
            final HttpsConfigurator configurator = new HttpsConfigurator(ssl) {
                @Override
                public void configure(final HttpsParameters params) {
                    final SSLParameters sslParameters = getSSLContext().getDefaultSSLParameters();
                    // sslParameters.setNeedClientAuth(true); // Client cert authentication
                    sslParameters.setCipherSuites(getSSLContext().getServerSocketFactory().getSupportedCipherSuites());
                    params.setSSLParameters(sslParameters);
                }
            };
            final HttpsServer httpsServer = HttpsServer.create(new InetSocketAddress(hostname, port), port);
            httpsServer.setHttpsConfigurator(configurator);
            HttpContext context = httpsServer.createContext("/ssl-service");
            endpoint.publish(context);
            httpsServer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
