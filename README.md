# JAX-WS Server Example

We're going to build a simple _hello world_ web service server using CXF for the Java code generation.

## Create empty project

Generate an empty Java project. I'll use IntelliJ to generate a new Gradle project.

[Grab a WSDL](https://duckduckgo.com/?q=hello+world+wsdl&ia=web) and save it in `src/main/resources`. You should have something similar to 88c940fd1a159e5f03bb718ecff215b1ddaa2c61.

## Generate JAX-WS server

Generate JAX-WS server with wsdl2java. This tool is part of Apache's CXF distribution, we can use sdkman to install it if needed.

### sdkman

Follow installation instructions from https://sdkman.io/install.

```shell script
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
sdk version
```

### CXF

Once installed sdkman, installing CXF is just running one command.

```shell script
sdk install cxf
```

Check the installation running `wsdl2java -h`.

### wsdl2java

```shell script
wsdl2java -p com.gitlab.isalgueiro.examples.jaxws.server -server -d src/main/java/ src/main/resources/WSCitationImplPortBinding.wsdl
```

This will generate all the needed Java classes to implement this service.

 - `-p` target package for the generated classes
 - `-server` Generates starting point code for a web service server.
 - `-d` directory into which the generated code files are written.

See results in commit 88c940fd1a159e5f03bb718ecff215b1ddaa2c61.

### Main method

CXF generated main method is a little bit simple. To deploy a more usable server we need to make a couple of changes:

1. Service port implementation. We can implement the generated interface and copy the `WebService` annotations there.
2. Use JAX-WS `Endpoint` to publish the service and keep it running listening to requests.

You should end with something like 208feb5d451f01248e3aea159406a57c5dc4ee2c.

## SSL!

### Self-signed testing keystore

```shell script
keytool -keystore src/main/resources/test.jks -genkey -keyalg RSA -alias test
```

### Edit main method

We need to load the keystore and use it to configure the HTTPS server.

See this new main method in bf9879ec59178ea7c9a7316ca5317ff301835682.
